#pragma once

#include <bitset>
#include <vector>
#include <random>

template <std::size_t N>
struct BGA_chromosome_t
{
    BGA_chromosome_t(unsigned long long val)
        : bits(val)
    {}

    BGA_chromosome_t(const std::bitset<N> & bits)
        : bits(bits)
    {}

    double value(double min, double max) const
    {
        return min + bits.to_ullong() * (max - min) / ((1 << N) - 1);
    }

    bool operator[] (std::size_t i) const
    {
        return bits[i];
    }

    typename std::bitset<N>::reference operator[] (std::size_t i)
    {
        return bits[i];
    }

    template <typename Generator>
    static BGA_chromosome_t<N> random_chromosome(Generator & generator)
    {
        std::uniform_int_distribution<unsigned long long> distribution;
        return distribution(generator);
    }

    static constexpr std::size_t genes()
    {
        return N;
    }

    std::bitset<N> bits;
};

struct BGA_simple_crossover
{
    template <typename Chromosome, typename Generator>
    std::vector<Chromosome> operator()
        (const Chromosome & first, const Chromosome & second, Generator & generator)
        const
    {
        std::uniform_int_distribution<std::size_t> genes_distribution(0, Chromosome::genes() - 1);
        std::size_t k = genes_distribution(generator);
        Chromosome c_first = first, c_second = second;

        for (std::size_t i = k; i < Chromosome::genes(); ++i)
        {
            c_first[i]  = second[i];
            c_second[i] = first[i];
        }

        return { c_first, c_second };
    }
};

struct BGA_mutation
{
    template <typename Chromosome, typename Generator>
    void operator() (Chromosome & chromosome, Generator & generator) const
    {
        std::uniform_int_distribution<std::size_t> distribution(0, Chromosome::genes() - 1);
        std::size_t k = distribution(generator);
        chromosome[k] = !chromosome[k];
    }
};
