cmake_minimum_required(VERSION 3.5)
project(evol)

function(add_report NUMBER)
    add_custom_target(HW${NUMBER}.pdf
    ALL
    COMMAND pandoc
        -s ${CMAKE_CURRENT_SOURCE_DIR}/HW${NUMBER}.md
        -o HW${NUMBER}.pdf
        --variable title=Лабораторная\ ${NUMBER},\ вариант\ 13
        --variable author=Пьянкова\ Юлия
        --variable listings=true
        --variable mainfont=Liberation\ Sans
        --template=${CMAKE_CURRENT_SOURCE_DIR}/default.latex
        --latex-engine=xelatex
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/HW${NUMBER}.md
            ${CMAKE_CURRENT_SOURCE_DIR}/HW${NUMBER}.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/default.latex
    SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/HW${NUMBER}.md
            ${CMAKE_CURRENT_SOURCE_DIR}/HW${NUMBER}.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/default.latex
)
set_target_properties(HW${NUMBER}.pdf PROPERTIES EXCLUDE_FROM_ALL 1)
endfunction(add_report)

set(CMAKE_CXX_STANDARD 14)

add_library(gnuplot-iostream INTERFACE)
target_sources(gnuplot-iostream INTERFACE
    ${CMAKE_CURRENT_SOURCE_DIR}/gnuplot-iostream/gnuplot-iostream.h
)
target_include_directories(gnuplot-iostream INTERFACE
    ${CMAKE_CURRENT_SOURCE_DIR}/gnuplot-iostream/
)
target_link_libraries(gnuplot-iostream INTERFACE
    boost_system boost_iostreams boost_filesystem
)

set(EVOL_HEADERS common.h chromosome.h plot.h)

add_executable(HW1 HW1.cpp BGA.h ${EVOL_HEADERS})
target_link_libraries(HW1 gnuplot-iostream)
add_report(1)

add_executable(HW2 HW2.cpp RGA.h ${EVOL_HEADERS})
target_link_libraries(HW2 gnuplot-iostream)
add_report(2)

add_executable(HW3 HW3.cpp BGA.h path.h ${EVOL_HEADERS})
target_link_libraries(HW3 gnuplot-iostream)
add_report(3)

add_executable(HW4 HW4.cpp expr.h ${EVOL_HEADERS})
target_link_libraries(HW4 gnuplot-iostream)
add_report(4)
