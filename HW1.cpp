#include "common.h"
#include "chromosome.h"
#include "BGA.h"
#include "plot.h"

#include <gnuplot-iostream.h>

#include <vector>
#include <random>
#include <algorithm>
#include <cmath>
#include <limits>
#include <iterator>
#include <iomanip>

template <std::size_t N>
std::vector<double> to_dbl_population
    (const population_t<BGA_chromosome_t<N>> & population, double min, double max)
{
    std::vector<double> result;

    std::transform(population.begin(), population.end(),
                   std::back_inserter(result),
                   [min, max] (const BGA_chromosome_t<N> & chromosome)
                   { return chromosome.value(min, max); });

    return result;
}

double f(double x)
{
    return (exp(-x) - exp(x)) * cos(x)
        /  (exp(x) + exp(-x));
}

double min = -5.0, max = 5.0;

template <int N>
void plot_population
    (const population_t<BGA_chromosome_t<N>> & population,
     const std::string & population_name)
{
    std::vector<double> xs = to_dbl_population<N>(population, min, max);
    std::sort(xs.begin(), xs.end());
    std::vector<std::pair<double, double>> xys;
    std::transform(xs.begin(), xs.end(), std::back_inserter(xys),
                   [] (double x) { return std::make_pair(x, f(x)); });

    std::vector<std::pair<double, double>> function_pts;
    for (double x = min; x < max; x += 0.1)
        function_pts.push_back(std::make_pair(x, f(x)));

    std::pair<double, double> best_pt
            = *std::max_element(xys.begin(), xys.end(),
                                [] (auto const & a, auto const & b)
                                { return a.second < b.second; });
    Gnuplot gp;
    gp << "set label 1 '(" << std::setprecision(2) << best_pt.first
       << ", " << best_pt.second << ")' "
       << "at " << best_pt.first << "," << best_pt.second
       << " center offset 0,1" << std::endl;
    gp << "set xrange [" << min << ":" << max << "]" << std::endl;
    gp << "set yrange [-2:2]" << std::endl;
    gp << "plot '-' with points title '" << population_name << "'"
       <<    ", '-' with lines  title 'function'"
       << std::endl;
    gp.send1d(xys);
    gp.send1d(function_pts);
}

template <std::size_t N>
double f_fitness(const BGA_chromosome_t<N> & c)
{
    return f(c.value(min, max));
}

constexpr std::size_t BITS_IN_CHROMOSOME = 20;

int main()
{
    double true_fitness;
    {
        std::vector<double> xs;
        for (auto x = min; x < max; x += 0.001)
            xs.push_back(x);
        true_fitness = max_value<double>(xs.begin(), xs.end(), f);
    }

    stop_predicate_t<BGA_chromosome_t<BITS_IN_CHROMOSOME>>
            stop_predicate(true_fitness);

    auto population
            = genetic_algo
                <BGA_chromosome_t<BITS_IN_CHROMOSOME>>(
                f_fitness<BITS_IN_CHROMOSOME>,
                plot_population<BITS_IN_CHROMOSOME>,
                stop_predicate,
                BGA_simple_crossover(),
                BGA_mutation()
                ).first;

    plot_population<BITS_IN_CHROMOSOME>(population, "final population");

    plot_function<std::size_t, std::size_t>
            (10, 100, 5,
             [&stop_predicate] (std::size_t population_size)
             {
                 stop_predicate.reset();
                 options_t opts;
                 opts.population_size = population_size;
                 return genetic_algo
                    <BGA_chromosome_t<BITS_IN_CHROMOSOME>>
                    (f_fitness<BITS_IN_CHROMOSOME>,
                     [] (auto p, auto s) {},
                     stop_predicate,
                     BGA_simple_crossover(), BGA_mutation(), opts)
                    .second;
             }, "steps(population size)", 50);
    plot_function<double, std::size_t>
            (0.0, 1.0, 0.01,
             [&stop_predicate] (double crossover_probability)
             {
                 stop_predicate.reset();
                 options_t opts;
                 opts.crossover_probability = crossover_probability;
                 return genetic_algo
                    <BGA_chromosome_t<BITS_IN_CHROMOSOME>>
                     (f_fitness<BITS_IN_CHROMOSOME>,
                      [] (auto p, auto s) {},
                      stop_predicate,
                      BGA_simple_crossover(), BGA_mutation(), opts)
                     .second;
             }, "steps(crossover probability)", 50);
    plot_function<double, std::size_t>
            (0.0, 1.0, 0.01,
             [&stop_predicate] (double mutation_probability)
             {
                 stop_predicate.reset();
                 options_t opts;
                 opts.mutation_probability = mutation_probability;
                 return genetic_algo
                    <BGA_chromosome_t<BITS_IN_CHROMOSOME>>
                    (f_fitness<BITS_IN_CHROMOSOME>,
                     [] (auto p, auto s) {},
                     stop_predicate,
                     BGA_simple_crossover(), BGA_mutation(), opts)
                    .second;
             }, "steps(mutation probability)", 50);
}
