Задание: найти максимум функции
$f = \frac {(\exp(-x)-\exp(x)) \cos(x)}{\exp(x)+\exp(-x)}$,
$x \in [-5,5]$

Параметры алгоритма:

* количество бит в хромосоме: $20$
* линейное ранжирование, $a = 1.5$
* одноточечный кроссовер
* одноточеченая мутация
* вероятность кроссовера $0.5$
* вероятность мутации $0.001$

![](images_1/0.png).
![](images_1/1.png).
![](images_1/2.png).
![](images_1/3.png).
![](images_1/4.png).
![](images_1/5.png).
![](images_1/6.png).
![](images_1/7.png).
![](images_1/8.png).
![](images_1/9.png).
![](images_1/10.png).
![](images_1/final.png).

![Зависимость числа поколений от размера популяции](images_1/population.png)

![Зависимость числа поколений от вероятности кроссинговера](images_1/crossover.png)

![Зависимость числа поколений от вероятности мутации](images_1/mutation.png)

\newpage

## Код генетического алгоритма на C++:

\lstinputlisting[language=C++]{BGA.h}
\lstinputlisting[language=C++]{chromosome.h}
