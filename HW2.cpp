#include "chromosome.h"
#include "plot.h"
#include "RGA.h"

#include <gnuplot-iostream.h>

#include <cmath>
#include <array>

double f(double x1, double x2)
{
    double a = 1.0, b = 5.1 / (4 * std::pow(M_PI, 2)),
           c = 5 / M_PI, d = 6,
           e = 10, f = 1 / (8 * M_PI);
    return a * std::pow(x2 - b * std::pow(x1, 2) + c * x1 - d, 2)
         + e *  (1 - f) * std::cos(x1) + e;
}

double f_fitness(const RGA_chromosome_t<2> & c)
{
    return -f(c[0], c[1]);
}

std::array<std::pair<double, double>, 2> ranges = { std::make_pair(-5., 10.), std::make_pair(0., 15.) };

std::string to_string(const std::tuple<double, double, double> & tuple)
{
    std::stringstream ss;
    ss << std::get<0>(tuple) << "," << std::get<1>(tuple) << "," << std::get<2>(tuple);
    return ss.str();
}

void plot_population
    (const population_t<RGA_chromosome_t<2>> & population,
     const std::string & population_name)
{
    double eps = 0.5;
    std::vector<std::vector<std::tuple<double, double, double>>> function_pts;
    for (double x = ranges[0].first; x < ranges[0].second; x += eps)
    {
        function_pts.push_back({});
        for (double y = ranges[1].first; y < ranges[1].second; y += eps)
            function_pts.back().push_back(std::make_tuple(x, y, f(x, y)));
    }

    std::vector<std::tuple<double, double, double>> xys;
    std::transform(population.begin(), population.end(), std::back_inserter(xys),
                   [] (auto x) { return std::make_tuple(x[0], x[1], f(x[0], x[1])); });

    std::tuple<double, double, double> best_pt
            = *std::min_element(xys.begin(), xys.end(),
                                [] (auto const & a, auto const & b)
                                { return std::get<2>(a) < std::get<2>(b); });

    Gnuplot gp;
    gp << "set xrange [" << ranges[0].first - 1 << ":" << ranges[0].second + 1 << "]" << std::endl;
    gp << "set yrange [" << ranges[1].first - 1 << ":" << ranges[1].second + 1 << "]" << std::endl;
    gp << "set label 1 '(" << std::setprecision(2) << to_string(best_pt) << ")' "
       << "at " << to_string(best_pt) << " center offset 0,5" << std::endl;
    gp << "splot " << gp.binFile1d(xys, "record")          << "title '" << population_name << "'"
       <<     ", " << gp.binFile2d(function_pts, "record") << "with lines  title 'function'"
       << std::endl;
}

options_t default_options()
{
    options_t options;
    options.population_size = 50;
    options.a = 1.5;
    options.crossover_probability = 0.5;
    options.mutation_probability = 0.001;
    return options;
}

int main()
{
    double true_fitness = 0.397887;
    stop_predicate_t<RGA_chromosome_t<2>> stop_predicate(-true_fitness);
    stop_predicate.max_steps = 100;
    stop_predicate.eps = 0.001;
    options_t options = default_options();

    genetic_algo<RGA_chromosome_t<2>>(
        f_fitness,
        plot_population,
        stop_predicate,
        RGA_blend_crossover(0.4),
        RGA_random_mutation<2>(ranges),
        options,
        [] (auto & generator) { return RGA_chromosome_t<2>::random_chromosome(ranges, generator); }
    );

    stop_predicate.eps = 0.002;
    plot_function<std::size_t, std::size_t>
            (10, 100, 5,
             [&stop_predicate] (std::size_t population_size)
             {
                 stop_predicate.reset();
                 options_t opts = default_options();
                 opts.population_size = population_size;
                 return genetic_algo<RGA_chromosome_t<2>>
                    (f_fitness,
                     [] (auto p, auto s) {},
                     stop_predicate,
                     RGA_blend_crossover(0.4), RGA_random_mutation<2>(ranges), opts,
                     [] (auto & generator) { return RGA_chromosome_t<2>::random_chromosome(ranges, generator); })
                    .second;
             }, "steps(population size)", 50);
    plot_function<double, std::size_t>
            (0.0, 1.0, 0.01,
             [&stop_predicate] (double crossover_probability)
             {
                 stop_predicate.reset();
                 options_t opts = default_options();
                 opts.crossover_probability = crossover_probability;
                 return genetic_algo<RGA_chromosome_t<2>>
                    (f_fitness,
                     [] (auto p, auto s) {},
                     stop_predicate,
                     RGA_blend_crossover(0.4), RGA_random_mutation<2>(ranges), opts,
                     [] (auto & generator) { return RGA_chromosome_t<2>::random_chromosome(ranges, generator); })
                     .second;
             }, "steps(crossover probability)", 50);
    plot_function<double, std::size_t>
            (0.0, 1.0, 0.01,
             [&stop_predicate] (double mutation_probability)
             {
                 stop_predicate.reset();
                 options_t opts = default_options();
                 opts.mutation_probability = mutation_probability;
                 return genetic_algo<RGA_chromosome_t<2>>
                    (f_fitness,
                     [] (auto p, auto s) {},
                     stop_predicate,
                     RGA_blend_crossover(0.4), RGA_random_mutation<2>(ranges), opts,
                     [] (auto & generator) { return RGA_chromosome_t<2>::random_chromosome(ranges, generator); })
                    .second;
             }, "steps(mutation probability)", 50);
}
