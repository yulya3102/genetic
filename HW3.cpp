#include "path.h"
#include "BGA.h"
#include "chromosome.h"
#include "plot.h"

#include <gnuplot-iostream.h>

#include <vector>
#include <array>
#include <iostream>
#include <fstream>
#include <thread>

const std::size_t VERTICES = 29;

void plot_path
    (const std::array<std::size_t, VERTICES> & path,
     const std::string & title,
     const std::array<std::pair<double, double>, VERTICES> & coordinates)
{
    std::vector<std::pair<double, double>> xys;
    std::transform(path.begin(), path.end(), std::back_inserter(xys),
                   [&coordinates] (auto x) { return coordinates[x]; });

    Gnuplot gp;
    gp << "plot " << gp.binFile1d(xys, "record")
       << "title '" << title << "' "
       << "with linespoints"
       << std::endl;
}

void plot_population
    (const population_t<chromosome_t<VERTICES>> & population,
     const std::string & population_name,
     const std::array<std::pair<double, double>, VERTICES> & coordinates)
{
    plot_path(population.front().value(), population_name, coordinates);
}

options_t default_options()
{
    options_t options;
    options.population_size = 1000;
    options.a = 1.5;
    options.crossover_probability = 0.7;
    options.mutation_probability = 0.5;
    return options;
}

int main(int argc, char ** argv)
{
    if (argc < 4)
    {
        std::cerr << "Usage: " << argv[0] << " <matrix> <coordinates> <best answer>" << std::endl;
        return EXIT_FAILURE;
    }
    std::string
            file_matrix = argv[1],
            file_coord = argv[2],
            file_best = argv[3];

    std::array<std::array<double, VERTICES>, VERTICES> matrix;
    {
        std::ifstream is(file_matrix);
        for (std::size_t i = 0; i < matrix.size(); ++i)
        {
            for (std::size_t j = 0; j < matrix[i].size(); ++j)
            {
                double x;
                is >> x;
                matrix[i][j] = x;
            }
        }
    }
    std::array<std::size_t, VERTICES> best_path;
    {
        std::ifstream is(file_best);
        for (std::size_t i = 0; i < best_path.size(); ++i)
        {
            std::size_t v;
            is >> v;
            best_path[i] = v - 1;
        }
    }
    std::array<std::pair<double, double>, VERTICES> coordinates;
    {
        std::ifstream is(file_coord);
        for (std::size_t i = 0; i < coordinates.size(); ++i)
        {
            double x, y;
            is >> x >> y;
            coordinates[i] = std::make_pair(x, y);
        }
    }

    double true_fitness = 0.0;
    for (std::size_t i = 1; i < best_path.size(); ++i)
        true_fitness += matrix[best_path[i - 1]][best_path[i]];

    path_length<VERTICES> f(matrix);
    auto f_fitness = [&f] (const chromosome_t<VERTICES> & c) { return -f(c); };
    stop_predicate_t<chromosome_t<VERTICES>> stop_predicate(true_fitness);
    stop_predicate.max_steps = 5000;

    auto r = genetic_algo<chromosome_t<VERTICES>>(
        f_fitness,
        [&f] (auto p, auto pn) { std::cout << pn << ", best path: " << f(p.front()) << std::endl; },
        stop_predicate,
        BGA_simple_crossover(),
        path_mutation(),
        default_options()
    );

    plot_path(r.first.front().value(), "population #" + std::to_string(r.second), coordinates);
    plot_path(best_path, "best path", coordinates);

    std::cout << f(r.first.front()) << std::endl;
    std::cout << true_fitness << std::endl;

    stop_predicate.max_steps_unchanged = 100;
    stop_predicate.eps = true_fitness / 10.;
    auto t1 = std::thread([&] ()
    {
        std::vector<double> error;
        std::size_t min = 100, max = 1100, step = 50;
        plot_function<std::size_t, std::size_t>
                (min, max, step,
                 [&] (std::size_t population_size)
                 {
                     stop_predicate.reset();
                     options_t opts = default_options();
                     opts.population_size = population_size;
                     auto r = genetic_algo<chromosome_t<VERTICES>>
                        (f_fitness,
                         [] (auto p, auto s) {},
                         stop_predicate,
                         BGA_simple_crossover(), path_mutation(), opts);
                     double abs_error = std::abs(true_fitness - f(r.first.front()));
                     error.push_back(abs_error / true_fitness);
                     return r.second;
                 }, "steps(population size)");
        plot_function<std::size_t, double>
                (min, max, step,
                 [&] (std::size_t population_size)
                {
                    std::size_t i = (population_size - min) / step;
                    return error[i];
                }, "error(population size)");
    });
    auto t2 = std::thread([&] ()
    {
        std::vector<double> error;
        double min = 0., max = 1., step = 0.05;
        plot_function<double, std::size_t>
                (min, max, step,
                 [&] (double crossover_probability)
                 {
                     stop_predicate.reset();
                     options_t opts = default_options();
                     opts.crossover_probability = crossover_probability;
                     auto r = genetic_algo<chromosome_t<VERTICES>>
                        (f_fitness,
                         [] (auto p, auto s) {},
                         stop_predicate,
                         BGA_simple_crossover(), path_mutation(), opts);
                     double abs_error = std::abs(true_fitness - f(r.first.front()));
                     error.push_back(abs_error / true_fitness);
                     return r.second;
                 }, "steps(crossover probability)");
        plot_function<double, double>
                (min, max, step,
                 [&] (double crossover_probability)
                {
                    std::size_t i = (crossover_probability - min) / step;
                    return error[i];
                }, "error(crossover probability)");
    });
    std::vector<double> error;
    double min = 0.0, max = 1.0, step = 0.05;
    plot_function<double, std::size_t>
            (min, max, step,
             [&] (double mutation_probability)
             {
                 stop_predicate.reset();
                 options_t opts = default_options();
                 opts.mutation_probability = mutation_probability;
                 auto r = genetic_algo<chromosome_t<VERTICES>>
                    (f_fitness,
                     [] (auto p, auto s) {},
                     stop_predicate,
                     BGA_simple_crossover(), path_mutation(), opts);
                 double abs_error = std::abs(true_fitness - f(r.first.front()));
                 error.push_back(abs_error / true_fitness);
                 return r.second;
             }, "steps(mutation probability)");
    plot_function<double, double>
            (min, max, step,
             [&] (double mutation_probability)
            {
                std::size_t i = (mutation_probability - min) / step;
                return error[i];
            }, "error(mutation probability)");
    t1.join();
    t2.join();
}
