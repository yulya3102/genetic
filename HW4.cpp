#include "expr.h"
#include "chromosome.h"
#include "plot.h"

#include <cmath>
#include <vector>
#include <memory>
#include <random>
#include <stdexcept>
#include <fstream>
#include <sstream>
#include <experimental/optional>
#include <functional>

double f(double x1, double x2)
{
    double a = 1, b = 5.1 / (4 * std::pow(M_PI, 2)),
           c = 5. / M_PI, d = 6.,
           e = 10., f = 1. / (8 * M_PI);
    return a * std::pow(x2 - b * std::pow(x1, 2) + c * x1 - d, 2)
         + e * (1 - f) * std::cos(x1)
         + e;
}

chromosome_t f_chromosome()
{
    auto binop = [] (const auto & op, const auto & x, const auto & y)
    {
        return std::shared_ptr<expr_t>(new binary_expr_t(op, x, y));
    };
    auto uop = [] (const auto & op, const auto & x)
    {
        return std::shared_ptr<expr_t>(new unary_expr_t(op, x));
    };
    auto nop = [] (const auto & op)
    {
        return std::shared_ptr<expr_t>(new nullary_expr_t(op));
    };
    std::shared_ptr<expr_t>
        a = nop(ONE),
        b = binop(DIV,
                  nop(FIVE),
                  binop(MULT,
                        nop(FOUR),
                        uop(SQR, nop(PI)))),
        c = binop(DIV,
                  nop(FIVE),
                  nop(PI)),
        d = nop(SIX),
        e = nop(TEN),
        f = binop(DIV,
                  nop(ONE),
                  binop(MULT,
                        nop(EIGHT),
                        nop(PI)));
    return chromosome_t{
        binop(
            PLUS,
            binop(PLUS,
                binop(
                    MULT,
                    a,
                    uop(
                        SQR,
                        binop(
                            PLUS,
                            binop(
                                MINUS,
                                nop(X2),
                                binop(
                                    MULT, b, uop(SQR, nop(X1)))),
                            binop(
                                MINUS,
                                binop(MULT, c, nop(X1)),
                                d)))),
                binop(
                    MULT,
                    e,
                    binop(
                        MULT,
                        binop(
                            MINUS,
                            nop(ONE),
                            f),
                        uop(COS, nop(X1))))),
            e)
    };
}

struct f_error
{
    template <typename C>
    double operator() (const C & c)
    {
        double result = 0;
        std::size_t n = 0;
        for (double x1 = min; x1 < max; x1 += step)
            for (double x2 = min; x2 < max; x2 += step)
            {
                ++n;
                result += std::pow(c(x1, x2) - f(x1, x2), 2);
            }

        return result / n;
    }

    double step;
    double min;
    double max;
};

void plot_chromosome(const chromosome_t & c, const std::string & n)
{
    std::ofstream out(n);
    out << "digraph G {" << std::endl
        << c.to_string()
        << "}" << std::endl;
}

void plot(const population_t<chromosome_t> & p, const std::string & n)
{
    plot_chromosome(p[0], n);
}

options_t default_options()
{
    options_t options;
    options.population_size = 500;
    options.a = 1.5;
    options.crossover_probability = 0.7;
    options.mutation_probability = 0.3;
    return options;
}

void plot_2d_function
    (double min, double max, double step,
     const std::function<double(double, double)> & f,
     const std::string & name)
{
    std::vector<std::vector<std::tuple<double, double, double>>> function_pts;
    for (double x = min; x < max; x += step)
    {
        function_pts.push_back({});
        for (double y = min; y < max; y += step)
            function_pts.back().push_back(std::make_tuple(x, y, f(x, y)));
    }

    Gnuplot gp;
    gp << "splot " << gp.binFile2d(function_pts, "record") << "with lines title '" << name << "'"
       << std::endl;
}

int main()
{
    plot_chromosome(f_chromosome(), "answer.dot");
    double min = -100.,
           max = 100.,
           step = 5.;
    f_error error_function{step, min, max};
    stop_predicate_t<chromosome_t> stop_predicate(0.0, 500);
    auto r = genetic_algo<chromosome_t>
            ([&error_function] (auto const & c) { return -error_function(c); },
             plot,
             stop_predicate,
             tree_crossover(), tree_mutation(),
             default_options());
    plot_2d_function(min, max, step, f, "f");
    plot_2d_function(min, max, step,
                     [&r] (double x, double y) { return r.first.front()(x, y); },
                     "population #" + std::to_string(r.second));
}
