#pragma once

#include <array>
#include <random>

template <std::size_t N>
struct RGA_chromosome_t
{
    double operator[] (std::size_t i) const
    {
        return value[i];
    }

    double & operator[] (std::size_t i)
    {
        return value[i];
    }

    template <typename Generator>
    static RGA_chromosome_t<N> random_chromosome
        (const std::array<std::pair<double, double>, N> & ranges,
         Generator & generator)
    {
        RGA_chromosome_t<N> result;
        for (std::size_t i = 0; i < N; ++i)
        {
            std::uniform_real_distribution<double>
                distribution(ranges[i].first, ranges[i].second);
            result.value[i] = distribution(generator);
        }
        return result;
    }

    static std::size_t genes()
    {
        return N;
    }

    std::array<double, N> value;
};

struct RGA_blend_crossover
{
    RGA_blend_crossover(double alpha)
        : alpha(alpha)
    {}

    template <typename Chromosome, typename Generator>
    std::vector<Chromosome> operator()
        (const Chromosome & first, const Chromosome & second, Generator & generator)
        const
    {
        Chromosome child;
        for (std::size_t i = 0; i < Chromosome::genes(); ++i)
        {
            auto min = std::min(first[i], second[i]),
                 max = std::max(first[i], second[i]),
                 I   = max - min;
            std::uniform_real_distribution<double>
                 distribution(min - I * alpha, max + I * alpha);
            child[i] = distribution(generator);
        }

        return { child };
    }

    double alpha;
};

template <std::size_t N>
struct RGA_random_mutation
{
    RGA_random_mutation(const std::array<std::pair<double, double>, N> & ranges)
        : ranges(ranges)
    {}

    template <typename Chromosome, typename Generator>
    void operator() (Chromosome & chromosome, Generator & generator) const
    {
        std::uniform_int_distribution<std::size_t>
            genes_distribution(0, Chromosome::genes() - 1);
        std::size_t k = genes_distribution(generator);
        std::uniform_real_distribution<double>
            distribution(ranges[k].first, ranges[k].second);
        chromosome[k] = distribution(generator);
    }

    std::array<std::pair<double, double>, N> ranges;
};
