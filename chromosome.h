#pragma once

#include <random>
#include <functional>
#include <algorithm>
#include <vector>
#include <limits>

template <typename Chromosome>
using population_t = std::vector<Chromosome>;

template <typename Chromosome, typename Fitness, typename Generator>
population_t<std::pair<Chromosome, double>> random_population
    (std::size_t size, Generator & generator,
     const std::function<Chromosome(std::mt19937 &)> & random_chromosome,
     const Fitness & fitness)
{
    population_t<std::pair<Chromosome, double>> result;
    std::generate_n(std::back_inserter(result), size,
                    [&] ()
    {
        while (true)
        {
            auto c = random_chromosome(generator);
            double f_c = fitness(c);
            if (!std::isnan(f_c))
                return std::make_pair(c, f_c);
        }
    });

    return result;
}

template <typename Chromosome>
struct selection_t
{
    selection_t(std::size_t population_size, double a)
    {
        std::vector<double> probabilities;
        for (std::size_t i = 0; i < population_size; ++i)
            probabilities.push_back(
                    (a - (a - (2 - a)) * i
                         / (population_size - 1))
                    / population_size);
        for (std::size_t i = 0; i < population_size; ++i)
        {
            prob_part_sums.push_back(0);
            for (std::size_t j = 0; j <= i; ++j)
                prob_part_sums.back() += probabilities[j];
        }
    }

    template <typename IIter, typename OIter, typename Generator>
    OIter operator()
        (const IIter & begin, const IIter & end,
         OIter out,
         Generator & generator) const
    {
        std::uniform_real_distribution<double> probability_distrubution(0, 1);
        std::sort(begin, end,
                  [] (auto const & x, auto const & y)
                  { return x.second > y.second; });
        for (std::size_t i = 0; i < std::distance(begin, end); ++i)
        {
            auto random_selected_prob
                = probability_distrubution(generator);
            auto random_selected_it
                = std::lower_bound
                    (prob_part_sums.begin(), prob_part_sums.end(),
                     random_selected_prob);
            std::size_t random_selected_index
                = random_selected_it - prob_part_sums.begin();
            *out = *(begin + random_selected_index);
            ++out;
        }
        return out;
    }

    std::vector<double> prob_part_sums;
};

struct crossover_t
{
    crossover_t(double crossover_probability)
        : crossover_probability(crossover_probability)
    {}

    template <typename IIter, typename OIter,
              typename Crossover, typename Generator>
    OIter operator()
        (IIter begin, const IIter & end, OIter out,
         const Crossover & crossover, Generator & generator) const
    {
        std::uniform_real_distribution<double> probability_distribution(0, 1);

        for (; begin < end; begin += 2)
        {
            if (probability_distribution(generator)
                    < 1 - crossover_probability)
                continue;

            auto children = crossover(begin->first, (begin + 1)->first, generator);
            std::copy(children.begin(), children.end(), out);
        }

        return out;
    }

    double crossover_probability;
};

struct mutation_t
{
    mutation_t(double mutation_probability)
        : mutation_probability(mutation_probability)
    {}

    template <typename Population, typename Mutation, typename Generator>
    void operator() (Population & population, const Mutation & mutate,
                     Generator & generator) const
    {
        std::uniform_real_distribution<double> probability_distribution(0, 1);
        for (auto & chromosome : population)
        {
            if (probability_distribution(generator)
                    < 1 - mutation_probability)
                continue;

            mutate(chromosome, generator);
        }
    }

    double mutation_probability;
};

struct options_t
{
    std::size_t population_size = 10;
    double a = 1.5;
    double crossover_probability = 0.5;
    double mutation_probability = 0.001;
};

template <typename Chromosome>
struct default_random_chromosome_generator_t
{
    template <typename Generator>
    Chromosome operator() (Generator & generator)
    {
        return Chromosome::random_chromosome(generator);
    }
};

template <typename Chromosome, typename Crossover, typename Mutation,
          typename ChromosomeGenerator = default_random_chromosome_generator_t<Chromosome>>
std::pair<population_t<Chromosome>, std::size_t> genetic_algo
    (const std::function<double(const Chromosome &)> & fitness_func,
     const std::function
        <void(const population_t<Chromosome> &, const std::string &)>
        & plot_func,
     const std::function<bool(double)> stop_predicate,
     const Crossover & crossover_algo,
     const Mutation & mutation_algo,
     const options_t & options = options_t(),
     const ChromosomeGenerator & random_chromosome = ChromosomeGenerator())
{
    std::mt19937 generator;
    std::vector<std::pair<Chromosome, double>> population
        = random_population<Chromosome>(options.population_size, generator,
                                        random_chromosome, fitness_func);
    selection_t<Chromosome> selection(options.population_size, options.a);
    crossover_t crossover(options.crossover_probability);
    mutation_t mutation(options.mutation_probability);

    {
        population_t<Chromosome> cs;
        std::transform(population.begin(), population.end(), std::back_inserter(cs),
                       [] (auto const & x) { return x.first; });
        plot_func(cs, "population #0");
    }
    for (size_t population_index = 1; ; ++population_index)
    {
        std::vector<std::pair<Chromosome, double>> selected;
        selection(population.begin(), population.end(),
                  std::back_inserter(selected), generator);
        population_t<Chromosome> new_individuals;
        crossover(selected.begin(), selected.end(),
                  std::back_inserter(new_individuals),
                  crossover_algo, generator);
        mutation(new_individuals, mutation_algo, generator);

        // extend
        std::transform(new_individuals.begin(), new_individuals.end(),
                       std::back_inserter(population),
                       [&fitness_func] (auto const & x)
                       { return std::make_pair(x, fitness_func(x)); });
        population.erase(
            std::remove_if(population.begin() + options.population_size, population.end(),
                           [] (const auto & x) { return std::isnan(x.second); }),
            population.end());
        std::sort(population.begin(), population.end(),
                  [] (auto const & x, auto const & y)
                  { return x.second > y.second; });

        // reduce
        population.erase(population.begin() + options.population_size,
                         population.end());

        {
            population_t<Chromosome> cs;
            std::transform(population.begin(), population.end(), std::back_inserter(cs),
                           [] (auto const & x) { return x.first; });
            plot_func(cs, "population #" + std::to_string(population_index));
        }

        if (stop_predicate(population.front().second))
        {
            population_t<Chromosome> cs;
            std::transform(population.begin(), population.end(), std::back_inserter(cs),
                           [] (auto const & x) { return x.first; });
            return std::make_pair(cs, population_index);
        }
    }
}

template <typename Chromosome>
struct stop_predicate_t
{
    stop_predicate_t(
            double true_fitness,
            std::size_t max_steps = 200,
            std::size_t max_steps_unchanged = 100,
            double eps = 0.001)
        : true_fitness(true_fitness)
        , max_steps(max_steps)
        , max_steps_unchanged(max_steps_unchanged)
        , eps(eps)
        , steps(0)
        , steps_unchanged(0)
        , last_fitness(std::numeric_limits<double>::min())
    {}

    bool operator() (double fitness)
    {
        if (fitness == last_fitness)
            ++steps_unchanged;
        else
        {
            steps_unchanged = 0;
            last_fitness = fitness;
        }

        if (steps_unchanged > max_steps_unchanged)
            return true;

        if (std::abs(fitness - true_fitness) < eps)
            return true;

        ++steps;
        if (steps > max_steps)
            return true;

        return false;
    }

    void reset()
    {
        steps = 0;
        steps_unchanged = 0;
    }

    double true_fitness;
    std::size_t max_steps;
    std::size_t max_steps_unchanged;
    double eps;
    std::size_t steps;
    std::size_t steps_unchanged; double last_fitness;
};
