#pragma once

#include <iterator>
#include <functional>
#include <algorithm>
#include <vector>

template <typename ReturnValue, typename Iterator>
double max_value
    (Iterator begin, Iterator end,
     const std::function<ReturnValue(const typename std::iterator_traits<Iterator>::value_type&)> & f)
{
    std::vector<ReturnValue> values;
    std::transform(begin, end, std::back_inserter(values), f);
    return *std::max_element(values.begin(), values.end());
}

template <typename X, typename Y>
Y mean_result(const X & x, const std::function<Y(X)> & f, std::size_t n)
{
    Y mean_result(0);
    for (std::size_t i = 0; i < n; ++i)
        mean_result += f(x);
    mean_result /= n;
    return mean_result;
}
