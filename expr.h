#pragma once

#include "chromosome.h"

#include <string>
#include <cmath>
#include <experimental/optional>
#include <random>
#include <sstream>
#include <memory>

enum binary_op
{
    PLUS, MINUS, MULT, DIV
};

std::string to_string(binary_op op)
{
    std::string result;
    switch (op)
    {
        case binary_op::PLUS:
            result = "(+)";
            break;
        case binary_op::MINUS:
            result = "(-)";
            break;
        case binary_op::MULT:
            result = "(*)";
            break;
        case binary_op::DIV:
            result = "(/)";
            break;
    }
    return result;
}

enum unary_op
{
    SQR, COS
};

std::string to_string(unary_op op)
{
    std::string result;
    switch (op)
    {
        case unary_op::SQR:
            result = "(^2)";
            break;
        case unary_op::COS:
            result = "cos";
            break;
    }
    return result;
}

enum nullary_op
{
    PI, X1, X2,
    ONE, FOUR, FIVE, SIX, EIGHT, TEN
};

std::string to_string(nullary_op op)
{
    std::string result;
    switch (op)
    {
        case nullary_op::PI:
            result = "pi";
            break;
        case nullary_op::X1:
            result = "x1";
            break;
        case nullary_op::X2:
            result = "x2";
            break;
        case nullary_op::ONE:
            result = "1";
            break;
        case nullary_op::FOUR:
            result = "4";
            break;
        case nullary_op::FIVE:
            result = "5";
            break;
        case nullary_op::SIX:
            result = "6";
            break;
        case nullary_op::EIGHT:
            result = "8";
            break;
        case nullary_op::TEN:
            result = "10";
            break;
    }
    return result;
}

template <typename Op>
std::size_t ops_count();

template <>
std::size_t ops_count<binary_op>()
{
    return 4;
}

template <>
std::size_t ops_count<unary_op>()
{
    return 2;
}

template <>
std::size_t ops_count<nullary_op>()
{
    return 9;
}

double eval(binary_op op, double l, double r)
{
    double result = 0;
    switch (op)
    {
        case binary_op::PLUS:
            result = l + r;
            break;
        case binary_op::MINUS:
            result = l - r;
            break;
        case binary_op::MULT:
            result = l * r;
            break;
        case binary_op::DIV:
            result = l / r;
            break;
    }
    return result;
}

double eval(unary_op op, double x)
{
    double result = 0;
    switch (op)
    {
        case unary_op::SQR:
            result = x * x;
            break;
        case unary_op::COS:
            result = std::cos(x);
            break;
    }
    return result;
}

std::experimental::optional<double> eval(nullary_op op)
{
    double result = 0;
    switch (op)
    {
        case nullary_op::X1:
        case nullary_op::X2:
            return {};
        case nullary_op::PI:
            result = M_PI;
            break;
        case nullary_op::ONE:
            result = 1;
            break;
        case nullary_op::FOUR:
            result = 4;
            break;
        case nullary_op::FIVE:
            result = 5;
            break;
        case nullary_op::SIX:
            result = 6;
            break;
        case nullary_op::EIGHT:
            result = 8;
            break;
        case nullary_op::TEN:
            result = 10;
            break;
    }
    return result;
}

template <typename Op, typename Generator>
Op random_op(Generator & generator)
{
    std::uniform_int_distribution<std::size_t>
            op_distribution(0, ops_count<Op>() - 1);
    return Op(op_distribution(generator));
}

template <typename Op>
std::string label(std::size_t i, Op op)
{
    std::stringstream ss;
    ss << "\"" << i << ": " << to_string(op) << "\"";
    return ss.str();
}

struct expr_t : std::enable_shared_from_this<expr_t>
{
    virtual std::shared_ptr<expr_t> copy() const = 0;

    virtual double operator() (double x1, double x2) = 0;

    virtual std::shared_ptr<expr_t> operator[] (std::size_t i) = 0;

    virtual std::size_t size() const = 0;

    virtual void mutate(std::mt19937 & gen) = 0;

    virtual void set(std::size_t i, const std::shared_ptr<expr_t> & other) = 0;

    virtual std::string to_string(std::size_t i) const = 0;

    std::experimental::optional<double> cached_const;
};

struct binary_expr_t : expr_t
{
    binary_expr_t(binary_op op,
            const std::shared_ptr<expr_t> & left,
            const std::shared_ptr<expr_t> & right)
        : op(op)
        , left(left)
        , right(right)
    {
        cache_const();
    }

    virtual std::shared_ptr<expr_t> copy() const override
    {
        binary_expr_t * result = new binary_expr_t(*this);
        result->left = left->copy();
        result->right = right->copy();
        return std::shared_ptr<expr_t>(result);
    }

    void cache_const()
    {
        if (left->cached_const && right->cached_const)
            cached_const = eval(op, *left->cached_const,
                                    *right->cached_const);
        else
            cached_const = {};
    }

    virtual double operator() (double x1, double x2) override
    {
        if (cached_const)
            return *cached_const;

        double l = (*left) (x1, x2),
               r = (*right)(x1, x2);

        return eval(op, l, r);
    }

    virtual void mutate(std::mt19937 & gen) override
    {
        op = random_op<binary_op>(gen);
        cache_const();
    }

    virtual std::shared_ptr<expr_t> operator[] (std::size_t i) override
    {
        if (i == 0)
            return shared_from_this();
        std::size_t left_size = left->size();
        if (i > left_size)
            return (*right)[i - left_size - 1];
        else
            return (*left)[i - 1];
    }

    virtual std::size_t size() const override
    {
        return left->size() + right->size() + 1;
    }

    virtual void set(std::size_t i, const std::shared_ptr<expr_t> & other) override
    {
        if (i == 0)
            throw std::runtime_error("can't set 'this' to another tree");
        i -= 1;
        if (i == 0)
            left = other;
        else
        {
            std::size_t left_size = left->size();
            if (i < left_size)
                set(i, other);
            else
            {
                i -= left_size;
                if (i == 0)
                    right = other;
                else
                    right->set(i, other);
            }
        }
        cache_const();
    }

    virtual std::string to_string(std::size_t i) const override
    {
        std::size_t left_size = left->size();
        std::stringstream ss;
        ss << "node" << i << " [ label = " << label(i, op) << " ]" << std::endl;
        ss << "node" << i << " -> node" << i + 1 << std::endl;
        ss << "node" << i << " -> node" << i + left_size + 1 << std::endl;
        ss << left->to_string(i + 1);
        ss << right->to_string(i + left_size + 1);
        return ss.str();
    }

    binary_op op;
    std::shared_ptr<expr_t> left, right;
};

struct unary_expr_t : expr_t
{
    unary_expr_t(unary_op op, const std::shared_ptr<expr_t> & child)
        : op(op)
        , child(child)
    {
        cache_const();
    }

    virtual std::shared_ptr<expr_t> copy() const override
    {
        unary_expr_t * result = new unary_expr_t(*this);
        result->child = child->copy();
        return std::shared_ptr<expr_t>(result);
    }

    void cache_const()
    {
        if (child->cached_const)
            cached_const = eval(op, *child->cached_const);
        else
            cached_const = {};
    }

    virtual double operator() (double x1, double x2) override
    {
        if (cached_const)
            return *cached_const;

        double c = (*child)(x1, x2);
        return eval(op, c);
    }

    virtual void mutate(std::mt19937 & gen) override
    {
        op = random_op<unary_op>(gen);
        cache_const();
    }

    virtual std::shared_ptr<expr_t> operator[] (std::size_t i) override
    {
        if (i == 0)
            return shared_from_this();
        return (*child)[i - 1];
    }

    virtual std::size_t size() const override
    {
        return 1 + child->size();
    }

    virtual void set(std::size_t i, const std::shared_ptr<expr_t> & other) override
    {
        if (i == 0)
            throw std::runtime_error("can't set 'this' to another tree");

        i -= 1;
        if (i == 0)
            child = other;
        else
            child->set(i, other);
        cache_const();
    }

    virtual std::string to_string(std::size_t i) const override
    {
        std::stringstream ss;
        ss << "node" << i << " [ label = " << label(i, op) << " ]" << std::endl;
        ss << "node" << i << " -> node" << i + 1 << std::endl;
        ss << child->to_string(i + 1);
        return ss.str();
    }

    unary_op op;
    std::shared_ptr<expr_t> child;
};

struct nullary_expr_t : expr_t
{
    nullary_expr_t(nullary_op op)
        : op(op)
    {
        cached_const = eval(op);
    }

    virtual std::shared_ptr<expr_t> copy() const override
    {
        return std::shared_ptr<expr_t>(new nullary_expr_t(*this));
    }

    virtual double operator() (double x1, double x2) override
    {
        if (cached_const)
            return *cached_const;

        switch (op)
        {
            case nullary_op::X1:
                return x1;
            case nullary_op::X2:
                return x2;
            default:
                break;
        }

        return *eval(op);
    }

    virtual void mutate(std::mt19937 & gen) override
    {
        op = random_op<nullary_op>(gen);
        cached_const = eval(op);
    }

    virtual std::shared_ptr<expr_t> operator[] (std::size_t i) override
    {
        if (i == 0)
            return shared_from_this();
        throw std::runtime_error("index " + std::to_string(i) + " is too large");
    }

    virtual std::size_t size() const override
    {
        return 1;
    }

    virtual void set(std::size_t i, const std::shared_ptr<expr_t> & other) override
    {
        throw std::runtime_error("can't set 'this' to another tree");
    }

    virtual std::string to_string(std::size_t i) const override
    {
        std::stringstream ss;
        ss << "node" << i << " [ label = " << label(i, op) << " ]" << std::endl;
        return ss.str();
    }

    nullary_op op;
};

template <typename Generator>
std::shared_ptr<expr_t> random_tree(std::size_t max_size, Generator & gen)
{
    std::uniform_real_distribution<double> prob_distr(0, 1);
    double p = 1. / 3.;
    if (max_size == 1 || p > prob_distr(gen))
        return std::shared_ptr<expr_t>
            (new nullary_expr_t(random_op<nullary_op>(gen)));
    max_size -= 1;
    max_size /= 2;
    std::shared_ptr<expr_t> child = random_tree(max_size, gen);
    if (0.5 > prob_distr(gen))
        return std::shared_ptr<expr_t>
            (new unary_expr_t(random_op<unary_op>(gen), child));
    return std::shared_ptr<expr_t>(
                new binary_expr_t(
                    random_op<binary_op>(gen),
                    child,
                    random_tree(max_size, gen)));
}

struct chromosome_t
{
    chromosome_t copy() const
    {
        return chromosome_t{tree->copy()};
    }

    template <typename Generator>
    static chromosome_t random_chromosome(Generator & generator)
    {
        std::size_t max_size = 1024;

        return chromosome_t{random_tree(max_size, generator)};
    }

    double operator() (double x1, double x2) const
    {
        return (*tree)(x1, x2);
    }

    std::shared_ptr<expr_t> operator[] (std::size_t i)
    {
        return (*tree)[i];
    }

    std::size_t size() const
    {
        return tree->size();
    }

    void crossover(std::size_t i, const chromosome_t & other, std::size_t j)
    {
        auto i_expr = (*tree)[i]->copy(),
             j_expr = (*other.tree)[j]->copy();
        tree->set(i, j_expr);
        other.tree->set(j, i_expr);
    }

    std::string to_string() const
    {
        return tree->to_string(0);
    }

    std::shared_ptr<expr_t> tree;
};

struct tree_crossover
{
    template <typename Generator>
    population_t<chromosome_t> operator()
        (const chromosome_t & a,
         const chromosome_t & b,
         Generator & generator)
    const
    {
        if (a.size() == 1 || b.size() == 1)
            return {b, a};

        std::uniform_int_distribution<std::size_t>
            a_subtree(1, a.size() - 1),
            b_subtree(1, b.size() - 1);
        auto a_copy = a.copy(),
             b_copy = b.copy();
        a_copy.crossover(a_subtree(generator), b_copy, b_subtree(generator));
        return {a_copy, b_copy};
    }
};

struct tree_mutation
{
    template <typename Generator>
    void operator() (chromosome_t & c, Generator & generator) const
    {
        std::uniform_int_distribution<std::size_t> distribution(0, c.size() - 1);
        std::size_t k = distribution(generator);
        c[k]->mutate(generator);
    }
};
