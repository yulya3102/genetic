#pragma once

#include <array>
#include <vector>
#include <random>
#include <algorithm>

template <std::size_t N>
struct chromosome_t
{
    std::array<std::size_t, N> value() const
    {
        std::array<std::size_t, N> result;
        std::vector<std::size_t> vertices;
        for (std::size_t i = 0; i < genes(); ++i)
            vertices.push_back(i);

        for (std::size_t i = 0; i < genes(); ++i)
        {
            auto v = vertices.begin() + path[i];
            result[i] = (*v);
            vertices.erase(v);
        }

        return result;
    }

    static chromosome_t<N> from_value(const std::array<std::size_t, N> & value)
    {
        chromosome_t<N> result;
        std::vector<std::size_t> vertices;
        for (std::size_t i = 0; i < genes(); ++i)
            vertices.push_back(i);
        for (std::size_t i = 0; i < genes(); ++i)
        {
            auto j_it = std::find(vertices.begin(), vertices.end(), value[i]);
            result[i] = std::distance(vertices.begin(), j_it);
            vertices.erase(j_it);
        }
        return result;
    }

    std::size_t operator[] (std::size_t i) const
    {
        return path[i];
    }

    std::size_t & operator[] (std::size_t i)
    {
        return path[i];
    }

    static std::size_t genes()
    {
        return N;
    }

    template <typename Generator>
    static chromosome_t random_chromosome(Generator & generator)
    {
        chromosome_t result;
        for (std::size_t i = 0; i < genes(); ++i)
        {
            std::uniform_int_distribution<std::size_t>
                distribution(0, genes() - i - 1);
            result[i] = distribution(generator);
        }
        return result;
    }

    std::array<std::size_t, N> path;
};

struct path_mutation
{
    template <typename Chromosome, typename Generator>
    void operator() (Chromosome & chromosome, Generator & generator) const
    {
        std::uniform_int_distribution<std::size_t>
            i_distribution(0, Chromosome::genes() - 1);
        std::size_t i = i_distribution(generator);
        std::uniform_int_distribution<std::size_t>
            distribution(0, Chromosome::genes() - i - 1);
        std::size_t j = distribution(generator);
        auto value = chromosome.value();
        std::swap(value[i], value[j]);
        chromosome = Chromosome::from_value(value);
    }
};

template <std::size_t N>
struct path_length
{
    path_length(const std::array<std::array<double, N>, N> & matrix)
        : matrix(matrix)
    {}

    double operator() (const chromosome_t<N> & c)
    {
        auto path = c.value();
        double length = 0.;
        for (std::size_t i = 0; i < path.size() - 1; ++i)
            length += matrix[path[i]][path[i + 1]];
        return length;
    }

    const std::array<std::array<double, N>, N> & matrix;
};
