#pragma once

#include "common.h"

#include <gnuplot-iostream.h>

#include <functional>

template <typename X, typename Y>
void plot_function
    (const X & min, const X & max,
     const X & step, const std::function<Y(X)> & f,
     const std::string & name,
     std::size_t repeat = 1)
{
    std::vector<std::pair<X, Y>> pts;
    for (X x = min; x < max; x += step)
        pts.push_back(std::make_pair(x, mean_result(x, f, repeat)));

    Gnuplot gp;
    gp << "set xrange [" << min << ":" << max << "]" << std::endl;
    gp << "plot '-' with lines title '" << name << "'" << std::endl;
    gp.send1d(pts);
}
